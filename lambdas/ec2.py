#!/usr/bin/env python2
from __future__ import print_function
import logging

import boto3


logger = logging.getLogger()
logger.setLevel(logging.INFO)


DEFAULT_PAGE_SIZE = 100
REGION = 'us-east-1'
client = boto3.client('ec2', region_name=REGION)


def get_ec2_instances(event, context):
    """
    AWS Lambda event handler
    """
    logger.info(event)
    logger.info(event['params'])
    instance_ids = event['params']['querystring']['instance_ids'].split(',')

    return fetch_ec2_instances(instance_ids)


def fetch_ec2_instances(instance_ids):

    resp = client.describe_instances(InstanceIds=instance_ids)

    instances = []

    keys = ['InstanceType', 'InstanceId', 'PublicDnsName', 'PublicIpAddress']

    for res in resp.get('Reservations', []):
        for inst in res.get('Instances', []):
            instances.append(
                {key:inst[key] for key in keys}
            )

    return instances
