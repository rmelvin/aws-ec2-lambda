#!/usr/bin/env python2
from __future__ import print_function

import datetime
import json
import logging
import pprint

import boto3

DEFAULT_PAGE_SIZE = 100
REGION = 'us-east-1'
client = boto3.client('ecs', region_name=REGION)


class dtencode(json.JSONEncoder):
    def default(self, obj):
        if type(obj) == datetime.datetime:
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)


def get_clusters_handler(event, context):
    """
    AWS Lambda event handler
    Return all ECS Cluster names
    """
    # TODO: get page size from context params
    # page_size = context.params.get('pageSize', DEFAULT_PAGE_SIZE)
    page_size = DEFAULT_PAGE_SIZE
    return list_clusters(client, page_size=page_size)


def list_clusters(ecs_client, page_size=DEFAULT_PAGE_SIZE):

    """ Fetch a list of the clusters

    :param ecs_client: a boto3 ECS client
    :type ecs_client: boto3.client
    :param page_size: number of clusters to return
    :type page_size: int
    :returns: a list of clusters
    """
    list_response_key = 'clusterArns'
    describe_response_key = 'clusters'
    arns = []
    resp = ecs_client.list_clusters(maxResults=page_size)
    arns += resp[list_response_key]
    next_token = resp.get('nextToken')

    while next_token:
        resp = ecs_client.list_clusters(nextToken=next_token)
        arns += resp[list_response_key]
        next_token = resp.get('nextToken')

    if arns:
        desc_resp = ecs_client.describe_clusters(clusters=arns)
    else:
        return []

    return [str(c['clusterName'])
            for c in desc_resp[describe_response_key]
            ]


def get_cluster_handler(event, context):
    """
    AWS Lambda event handler
    Return all the data for a single ECS Cluster
    """
    NAME_PARAM = 'name'  # As defined in Swagger
    cluster_name = event['params']['path'][NAME_PARAM]
    return get_cluster(client, cluster_name)


def get_cluster(ecs_client, cluster_id):
    """
    Return all the data for a single ECS Cluster

    This massages the data into the following format:
    {
        "clusterName": "blah",
        "clusterArn": "blah",
        "instances": [
            {
                "containerInstanceArn": "foo",
                "ec2InstanceId": "bar",
                "tasks": [
                    {
                        "serviceName": "blah", //could be null
                        "serviceArn": "blah", //could be null
                        "taskDefinitionArn": "",
                        "taskArn": "",
                        "status": "",
                        "containers": [
                            {
                                "name": "",
                                "networkBindings": //same as boto,
                                "lastStatus": "",
                                "exitCode": "",
                                "taskArn": "",
                                "containerArn": "",
                            },
                            ...
                        ]
                    }
                    ...
                ]
            }
            ...
        ]
    }

    :param ecs_client: a boto3 ECS client
    :type ecs_client: boto3.client
    :param cluster_id: cluster name/id
    :type cluster_id: str
    """
    cl = describe_cluster(ecs_client, cluster_id)
    if not cl:
        return

    cluster_name = str(cl['clusterName'])
    instances = get_containers(client, cluster_name)
    services = get_services(client, cluster_name)
    for instance in instances:

        # Get tasks per a cluster/container combo
        tasks = get_tasks(
            client,
            str(cl['clusterName']),
            container_id=str(instance['containerInstanceArn']),
        )

        # map a specific service to each task
        for task in tasks:
            # print('finding service for task {}'.format(task['taskArn']))
            # TODO: for speedup, delete service from list once it's matched
            # to a task
            for service in services:
                if str(task['taskDefinitionArn']) == str(service['taskDefinition']):
                    # print('found match: task {} service {}'.format(
                    # task['taskArn'], service['serviceName']))

                    # bring over some service values to task
                    for k in ['serviceName', 'serviceArn']:
                        task[k] = service[k]

                    break

        instance['tasks'] = tasks
    cl['instances'] = instances
    return json.loads(json.dumps(cl, cls=dtencode))


def describe_cluster(ecs_client, cluster_id, page_size=DEFAULT_PAGE_SIZE):

    """ Fetch info on a cluster

    :param ecs_client: a boto3 ECS client
    :type ecs_client: boto3.client
    :param cluster_id: cluster name/id
    :type cluster_id: str
    :param page_size: number of clusters to return
    :type page_size: int
    :returns: a list of clusters
    """
    if not cluster_id:
        return

    desc_resp = ecs_client.describe_clusters(clusters=[cluster_id])
    if 'clusters' not in desc_resp:
        return

    if len(desc_resp['clusters']) < 1:
        return

    return desc_resp['clusters'][0]


def get_containers(ecs_client, cluster_id, page_size=DEFAULT_PAGE_SIZE):
    """ Fetch a list of the container instances

    :param ecs_client: a boto3 ECS client
    :type ecs_client: boto3.client
    :param cluster_id: cluster name/id
    :type cluster_id: str
    :param page_size: number of container instances to return
    :type page_size: int
    :returns: a list of containers
    """
    list_response_key = 'containerInstanceArns'
    describe_response_key = 'containerInstances'
    arns = []
    resp = ecs_client.list_container_instances(
        cluster=cluster_id,
        # maxResults=page_size,
    )
    arns += resp[list_response_key]
    next_token = resp.get('nextToken')

    while next_token:
        resp = ecs_client.list_container_instances(nextToken=next_token)
        arns += resp[list_response_key]
        next_token = resp.get('nextToken')

    if arns:
        desc_resp = ecs_client.describe_container_instances(
            cluster=cluster_id,
            containerInstances=arns,
        )
    else:
        return []

    return desc_resp[describe_response_key]


def get_services(ecs_client, cluster_id, page_size=DEFAULT_PAGE_SIZE):
    """ Fetch cluster services

    :param ecs_client: a boto3 ECS client
    :type ecs_client: boto3.client
    :param cluster_id: cluster name/id
    :type cluster_id: str
    :param page_size: number of services to return
    :type page_size: int
    :returns: a list of cluster services
    """
    list_response_key = 'serviceArns'
    describe_response_key = 'services'
    arns = []
    resp = ecs_client.list_services(
        cluster=cluster_id,
        # maxResults=page_size,
    )
    arns += resp[list_response_key]
    next_token = resp.get('nextToken')

    while next_token:
        resp = ecs_client.list_services(nextToken=next_token)
        arns += resp[list_response_key]
        next_token = resp.get('nextToken')

    if arns:
        desc_resp = ecs_client.describe_services(
            cluster=cluster_id,
            services=arns,
        )
    else:
        return []

    return desc_resp[describe_response_key]


def get_tasks(ecs_client, cluster_id, container_id=None, service_id=None,
              family=None, desired_status=None,
              page_size=DEFAULT_PAGE_SIZE):
    """ Fetch tasks for a given cluster/container/service/family

    :param ecs_client: a boto3 ECS client
    :type ecs_client: boto3.client
    :param cluster_id: cluster name/id
    :type cluster_id: str
    :param container_id: container instance name/id
    :type container_id: str
    :param service_id: service name/id
    :type service_id: str
    :param family: family name/id
    :type family: str
    :param desired_status: desired status
    :type desired_status: str i.e. 'RUNNING'|'PENDING'|'STOPPED'
    :param page_size: number of tasks to return
    :type page_size: int
    :returns: a list of tasks for a cluster/container/service/family
    """
    list_response_key = 'taskArns'
    describe_response_key = 'tasks'
    arns = []
    list_task_kwargs = {
        'cluster': cluster_id
    }
    if container_id:
        list_task_kwargs.update({'containerInstance': container_id})

    if service_id:
        list_task_kwargs.update({'serviceName': service_id})

    if family:
        list_task_kwargs.update({'family': family})

    if desired_status:
        list_task_kwargs.update({'desiredStatus': desired_status})

    resp = ecs_client.list_tasks(**list_task_kwargs)
    arns += resp[list_response_key]
    next_token = resp.get('nextToken')

    while next_token:
        resp = ecs_client.list_tasks(nextToken=next_token)
        arns += resp[list_response_key]
        next_token = resp.get('nextToken')

    if arns:
        desc_resp = ecs_client.describe_tasks(
            cluster=cluster_id,
            tasks=arns,
        )
    else:
        return []

    return desc_resp[describe_response_key]


if __name__ == "__main__":
    clusters = list_clusters(client)
    for cid in clusters:
        cl = get_cluster(client, cid)
        pprint.pprint(cl)
